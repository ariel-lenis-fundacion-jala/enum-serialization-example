﻿using EnumBodyExample.Domain;
using Microsoft.AspNetCore.Mvc;

namespace EnumBodyExample.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BooksController : ControllerBase
    {
        [HttpGet]
        public Book Get()
        {
            return new Book()
            {
                Title = "Hiperspace",
                Author = "Michio Kaku",
                Category =  Categories.Science
            };
        }

        [HttpPost]
        public Book Post([FromBody] Book value)
        {
            return value;
        }
    }
}
