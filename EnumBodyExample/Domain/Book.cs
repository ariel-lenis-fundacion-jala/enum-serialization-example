﻿namespace EnumBodyExample.Domain
{
    public class Book
    {
        public string Title { get; set; }
        public string Author { get; set; }
        public Categories Category { get; set; }
    }
}
